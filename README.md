# Conventional Commits
 ## `<type>[optional scope]: <english description. Use the imperative, and don't capitalize the first letter>`
#
 ### **Breaking Change**
 * Appends a **!** after the type/scope. A BREAKING CHANGE can be part of commits of any type.

### **Type**
* build: Changes that affect the build system or external dependencies (example * scopes: gulp, broccoli, npm)
* ci: Changes to our CI configuration files and scripts (example scopes: * Travis, Circle, BrowserStack, SauceLabs)
* docs: Documentation only changes
* feat: A new feture
* fix: A bug fix
* perf: A code change that improves performance
* refactor: A code change that neither fixes a bug nor adds a feature
* style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* test: Adding missing tests or correcting existing tests


